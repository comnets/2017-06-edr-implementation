
\newcommand{\CLASSINPUTtoptextmargin}{1cm}
\newcommand{\CLASSINPUTbottomtextmargin}{1cm}
\documentclass[conference, a4paper]{IEEEtran}

\IEEEoverridecommandlockouts

\ifCLASSINFOpdf
\else
\fi

\input{./header.tex}

\setlength{\textfloatsep}{10pt plus 1.0pt minus 2.0pt}
\newcommand{\card}[1]{\ensuremath{\left\lvert #1 \right\rvert}}

\include{pics}

\begin{document}

\newtheorem{algorithm}{Algorithm}
\newtheorem{method}{Sorting method}

\setlength{\abovedisplayshortskip}{0pt}
\setlength{\belowdisplayshortskip}{0pt} 

\title{Cooperation Group Size in Opportunistic Wireless Mesh: Optimal Versus Practical}
% \title{Too Big, Too Small - Size Doesn't Matter After All in Opportunistic Wireless Mesh Networks}

\author{\IEEEauthorblockN{Sreekrishna Pandi\IEEEauthorrefmark{1}, Ievgenii Tsokalo\IEEEauthorrefmark{1}, Simon Wunderlich\IEEEauthorrefmark{1}, and Frank H.P. Fitzek\IEEEauthorrefmark{1}}
\IEEEauthorblockA{\IEEEauthorrefmark{1}Deutsche Telekom Chair of Communication Networks\\
Technische Universit\"at Dresden, 01062 Dresden, Germany\\
Email: \{firstname\}.\{lastname\}@tu-dresden.de
}
}

% make the title area
\maketitle

\begin{abstract}

This paper investigates the performance of wireless mesh networks advocating for
Opportunistic Routing (OpR) over traditional Single Path Routing  (SPR). In OpR,
all nodes overhearing the transmission can form a cooperation group and forward
the fragments of the received message. The bigger the cooperation group, the
more is the message sub-divided and the harder it gets to manage this process.
If poorly managed, multiple duplicate fragments can be forwarded that downgrades
the overall performance and destabilizes the routing protocol. In this paper, we
propose several methods for shrinking the cooperation group and define the best
one that can be later used as a part of the routing protocol. Using a testbed of
15 node WiFi network deployed in the office building, we show that the
cooperation group size can be significantly reduced using our proposed methods.
The throughput gain of OpR over SPR reaches as high as 51\% in our testbed.  We
also show that only 3\% of the average gain is lost when shrinking the
cooperation group to less than one-third of its original size. The conducted
measurement and the analysis of the results form the benchmark for potential
performance evaluation of the mesh networks.

\end{abstract}

\begin{IEEEkeywords}Opportunistic routing, effective data rate, wireless mesh \end{IEEEkeywords}


\IEEEpeerreviewmaketitle


\section{Introduction}

Most of the wireless mesh networks nowadays use the Single-Path Routing (SPR),
where the routes are planned before the data transmission starts, e.g. AODV
\cite{aodv}, OLSR \cite{olsr}, DSR \cite{DSR}, B.A.T.M.A.N \cite{batman}, etc.
Using channel quality estimation and the exchange of the dedicated service
messages, a unique route is computed for each source-destination pair such that
the message transmitted by each node is forwarded by exactly one node,
southbound in the route. In Opportunistic Routing (OpR) the routes are not
planned beforehand. Instead, the nodes take autonomous decisions when the data
should be routed based on diverse information like channel quality, previously
received feedbacks, previously received/sent data, etc. OpR exploits the
broadcast nature of the wireless medium.

Although OpR is in an emerging phase, multiple studies
\cite{soar,more,ccack,eatt,eax,orteorstudy,uniorunicast,orcd,
	codecast,PlayNCool,SPDynamicWM,AdaptiveRelayPlayNCool}
have showcased the advantages of OpR over SPR. The key idea of these studies
lies in the better exploitation of the channel capacity. As shown in \cite{diss,
	approachinglimits, more}, SPR cannot achieve the highest possible data rate in
the networks with a broadcast lossy channel. On contrary, OpR approaches this
limit showing positive gain against SPR for any network topologies. Despite the
guaranteed throughput gains, the more interesting advantages of OpR lies in its
potential to minimize the latency and maximize the resilience of the wireless
mesh networks. This is achieved by routing over multiple paths and intelligently
adding redundancy in each path.

The autonomous routing decision making process of OpR introduces new challenges
the the protocol design. In order to achieve optimal channel utilization (by
avoiding more than one node forwarding the same transmission), only one of all
the nodes that "heard" the transmission should forward it down the path. Some
studies \cite{uncodedOpR} achieve this by extensive signaling and control messages.
However, it has been proven in \cite{diss} that the use of Network
Coding~\cite{AhlswedeNetFlows} in OpR can achieve optimal channel capacity
without the rigorous signaling mechanisms. In Network Coded OpR (NCOpR), the
transmission from each node is "forwarded" down stream by a set of nodes,
commonly called a \textit{cooperation group}, each with a certain probability.
Forwarding in NCOpR is essentially retransmitting a linear combination of the
latest packets of the said transmission stream. The probability of forwarding of
each node may be adjusted by the routing protocol so as to overcome any
estimated losses in the link (Forward Error Correction) or to even provide
increased resilience against complete relay node failures by having a
sufficiently high forwarding probability. Figs.~\ref{fig:exampleroutingspr}
and~\ref{fig:exampleroutingopr} illustrate the fundamental differences between
SPR and OpR.

One of the key challenges in OpR is identifying the right set of cooperation
group for each sender in the stream. Of course, the nodes in the network that
did not receive the transmission from the sender cannot be a part of the
cooperation group. \cite{diss} shows that only certain nodes that received the
transmission should forward it in order to positively contribute to increase the
throughput of the stream. Therefore, multiple theoretical studies \cite{diss,
	more} propose that the cooperation group of each sender (or relay) include all
the nodes that can potentially increase the overall throughput of the stream,
even by a little bit. This results in a relatively large sizes
of cooperation groups. Theoretically, the cooperation gain is guaranteed to
increase with the increase in cooperation group size (CGS). However, in
practice, the stability of the routing protocol decreases with increase in
cooperating nodes, thus also decreasing the throughput. This is because, the
number of random processes (individual link errors, congestion, etc.) based on
which the routing decisions are taken, increase polynomially with the number of
cooperating nodes. The number of cooperating nodes, i.e. CGS may be artificially
limited in order to design practical OpR protocols.

The main goal of this work is to analyze how much of the OpR gain is lost by
limiting the CGS to a certain higher bound. This would help us to engineer
practical OpR protocols in the future work. However, selecting the ``right''
subset of nodes among the all the nodes in the cooperating group is a
significant challenge. The choice of the subset can have substantial impact on
the resulting decrease in OpR gain. In this paper, we propose multiple methods
to limit the CGS with respective node selection criteria. We study the impact of
each method on the gain of OpR over SPR. We also evaluate the effect of the
degree of CGS reduction on the OpR gains.

Designing a simulation environment for developing and analyzing OpR protocols is
not trivial. The performance of OpR protocols depend heavily on the spatial
diversity of the physical medium and the correlation of the packet losses
between different receivers~\cite{wifiLossCorrel}. Therefore we
do not perform evaluation using network simulators. Instead, we use actual
packet traces from a real WiFi deployment. For this purpose, we set up a 15 node
WiFi testbed in the office premises to gather trace data on WiFi channel
conditions and packet loss characteristics over a long time period. All the
evaluations conducted in the paper are performed using these trace data.


The overall structure of the rest of the paper are as follows. The
section~\ref{sec:mnat} elaborates the testbed we use to generate the trace data.
It also describes how the data is organized and represented for further
analysis. Section~\ref{sec:gain_def} introduces a metric to help us evaluate the
multiple CGS shrinking methods in the later sections. In
section~\ref{sec:stratergies}, we describe six different functions for computing
the cooperation group by each sender. In the following section, we evaluate the
proposed CGS shrinking functions using the proposed gain metric and identify the
pros and cons of each function.


\begin{figure}[!t]
    \centering
    \multirelayOnePath    
    \caption[caption]{Example: Routing in mesh with SPR}
    \label{fig:exampleroutingspr}
\end{figure}

\begin{figure}[!t]
    \centering
    \multirelayOpR    
    \caption[caption]{Example: Routing in mesh with OpR}
    \label{fig:exampleroutingopr}
\end{figure}

% \begin{figure}[!t]
%     \centering
%     \sets
%     \caption[caption]{Adopted notations}
%     \label{fig:sets}
% \end{figure}
% 
% \todot{Fig~\ref{fig:sets} needs to look more reader friendly. Add words if it helps}
% 
% \textit{Adopted notations (see fig.~\ref{fig:sets})}: In the network graph $\mathcal{G}(V,E)$, $V$ is the
% set of nodes and $E$ is the set of edges. We define the set $L(v)\subseteq V$ as
% such that $u\in L(v)$ can decode at least one codeword on the physical layer
% (PHY) sent by $v$ using the most robust Modulation and Coding Scheme (MCS). We
% also refer to $L(v)$ as the \textit{audible cooperation group}. With the subset
% $L'(v)\subseteq L(v)$ we denote the audible cooperation group after removing
% some nodes. Eventually, not all audible nodes can be useful for the
% performance enhancement. We define $L(v)\subseteq L(v)$ as the useful
% cooperation group and $L'(v)\subseteq L(v)$ as the useful cooperation group
% after removing the less performant nodes. Note that $L(v)$ is one of possible
% $L'(v)$.

\section{Testbed Description}
\label{sec:mnat}

We deployed a 15 node IEEE802.11n based wireless mesh testbed in the office
premises. The nodes were evenly distributed geographically as shown in
fig.~\ref{fig:topology}. Each test node is comprised of a PC Engines APU1D4,
running OpenWRT provisioned with a Compex WLE200NX a/b/g/n WiFi card attached
with a 5db antenna. The WiFi card is driven by the \textit{ath9k} driver, which
provides many necessary low-level features such as manually setting the 
Modulation and Coding Scheme (MCS) \cite{mcsindex} per individual packet,
turning off the MAC level retransmissions, etc.


\begin{figure}[!t]
    \centering
    \includegraphics[width=2.2in]{imgs/map.pdf}
    \caption[caption]{Testbed topology. Scale: The distance from $node~2$ to
    $node~14$ is 46 meters.}
    \label{fig:topology}
\end{figure}

We use \textit{Ratechecker} \cite{ratechecker} as our evaluation tool. It is
 designed to send packets in different MCS rates with random
intervals while constantly logging all packets overheard from other nodes
running the ratechecker program. Each node sequentially sends a burst of 64
packets, each 1400 bytes long. We send sufficiently large number of packets in
order to achieve over 95\% confidence interval in our evaluation results. A
Cyclic Redundancy Code (CRC) is attached to each packet by the underlying IEEE
802.11 MAC layer~\cite{ieee80211}. All nodes that can hear the transmission perform
the CRC check. In case of success, the receiver records ``1'' and, in case the
CRC check failed or if the packet is completely lost in the channel, ``0'' is
recorded. The sequences of bits are saved in a database for later evaluation.
Each burst is sent using a randomly chosen MCS scheme so that the channel
qualities for all the possible MCS schemes are evenly gauged.

The ratechecker tool guarantees that only one sender transmits at a time with a
probability $> 99.9$\% . Thus, the nodes do not interfere with each other.

At the end of the measurement, the obtained logs are collected at a central
computer. They comprise the database, where we can find out the reception status
$s[v][k][u][j][i]$ of the $i$th packet in the $j$th bunch sent by node $v$ using
MCS $k$ and received by node $u$. We perform the post-processing of this
database to obtain all results in this paper.

\section{Evaluation of OpR over SPR gain}
\label{sec:gain_def}

The OpR uses the spatial diversity of the broadcast communication channel to
increase the throughput. The diversity of the channel is achieved through the
spatial separation of the receivers. For example, consider the node $v$ in the
fig.~\ref{fig:exampleroutingopr}. Any packet transmitted by $v$ may be received
by either or both of the nodes $u_{1}$ and $u_{2}$. If the nodes  $u_{1}$ and
$u_{2}$ are sufficiently spatially separated, they will have diverse channel
conditions. i.e. if a packet is lost on one of the links, it is less likely that
it will also be lost on the other link.

Using the traces of the real WiFi network, we can evaluate the amount of
diversity by analyzing the packet losses and correlation of losses between
individual sender-receiver pairs. We generate a network graph from the traces.
We evaluate the highest achievable throughput with SPR, $r(v)$ by  applying
Dijkstra's algorithm on the network graph. The quality of each point-to-point
link is defined with the corresponding traces $s[v][k][u]$ (see
section~\ref{sec:mnat}). We evaluate  the throughput with OpR as the priority
$p(v)$ using the same traces and applying the algorithm proposed in
\cite{approachinglimits}. We introduce it shortly here. The value $p(v)$ is also
referred to as the node
\textit{priority}.


The major performance metric in this paper is the gain between OpR and SPR:
\begin{equation}
\label{eq:gain}
 g(v) = (p(v) - r(v)) / r(v),
\end{equation}

\subsection{Evaluation of the throughput with SPR}

For evaluation of the throughput with SPR, $r(v)$, we apply Dijkstra's
algorithm, which requires the prior assignment of the weights to each edge of
the network graph. For the edge between each given sender $v$ and receiver $u$,
$v,u\in V$, we find the edge weight as the inverse of the effective
point-to-point data rate $R(v,u)=\max_{k\in K}[d_k(v)\cdot (1 -
\varepsilon_{(v,u)})]$, where $d_k(v)$ is the sending data rate corresponding to
the MCS $k\in K$ and $\varepsilon_{(v,u)}$ is the loss ratio on the edge
$(v,u)$. $K$ is the set of all considered MCSs. The obtained traces from the
testbed contain the measurement data for all MCS that IEEE802.11n and
IEEE802.11ac support \cite{mcsindex}. Note that with the less robust MCS, the
sending data rate $d_k(v)$ becomes higher but the loss ratio
$\varepsilon_{(v,u)}$ grows as well creating a tradeoff for the MCS selection.
The value $[R(v,u)]^{-1}$ is assigned to the edge $(v,u)$. If certain $v$ and
$u$ are not reachable directly then $R(v,u) = 0$. At this point, the traditional
Dijkstra's algorithm can be applied.



\subsection{Evaluation of the throughput with OpR}


In \cite{approachinglimits}, the authors propose two standalone approaches to
evalute the \textit{priority}. The first one relates to the evaluation of the
channel capacity of the network with the broadcast lossy erasure channel with
time-division multiplexing. It was also partially described in
\cite{Khojastepour2003, CheapNodes}. The disadvantage of such approach lies in
the necessity of the global knowledge of the network graph. Another approach
provides the algorithm for the priority $p(v)$ calculation that can be applied
in a decentralized fashion. Hence, it is more attractive for the application in
mesh networks. Therefore, we use it also in our work. We evaluate the throughput
with OpR as the priority using the following expression
\cite{approachinglimits}:
\begin{equation}
 \label{eq:prioirtymain}
 p(v)=\frac{a(v)}{1 + \sum\limits_{u\in L(v)\setminus w}b(u,v)/p(u)},
\end{equation}
where $w$ is the destination, the coefficient $a(v)$ describes the amount of the
information flow over cut between $v$ and $L(v)$, and $b(u,v)$ describes the
amount of information that will be forwarded by $u$ when $v$ sends one unit of
information.

The priority value, eq.~\ref{eq:prioirtymain}, has the data rate units. It
basically equals the achievable data rate between the node $v$ and the
destination when using all possible communication paths. The value $a(v)$ equals
the data rate between $v$ and its cooperation group $L(v)$. The ratio
$b(u,v)/p(u)$ evaluates the ``goodness'' of the connection between the
cooperating node $u$ and the destination. The better it is the less is the ratio
and the greater is $p(v)$. This ratio also evaluates the contribution of the
node $u$ into the overall communication.

In \cite{approachinglimits}, the general equations for $a(v)$ and $b(u,v)$ are
provided. We apply these equations on our database values. As a result, they can
be calculated as follows:
\begin{equation}
\label{eq:aandb}
\begin{aligned}
  \ & a(v)=d(v)\cdot [1-(1 -||\cup_{u\in L(v)}bs_{(v,u)}||^2/l)];\\
  \ & b(u,v)=d(v)\cdot (1-(1 -||bs_{(v,u)}||^2/l))\cdot\\
   \ & \ \ \ \ \ \ \ \ \ \ \ (1 -||\cup_{u'\in X'(v,u)}bs_{(v,u')}||^2/l),
\end{aligned}
\end{equation}
where all $bs_{(v,u)}$ vectors have the same length $l$ and $X(v,u)$ is a 
subset of $L(v)$ so that $\forall u'\in X(v,u):\ p(u)<p(u')$.

You can see from eq.~\ref{eq:prioirtymain}, that the priority of $v$ depends on
the priorities of its neighbors. Also, $p(v)$ depends on $L(v)$ and in the same
time $L(v)$ depends on $p(v)$ and $p(u)\ \forall u\in L(v)$. Thus, the
evaluation of $p(v)\ \forall v\in V$ is an iterative process. The number of
iterations depends on the level of interconnection of the nodes. It is the
highest for the full mesh networks. Still, the calculation of $p(v)$ can be
realized in the decentralized fashion \cite{approachinglimits}.


\section{Strategies for selection of cooperating nodes}
\label{sec:stratergies}

The priority $p(v)$ is dependent on how many and which nodes do cooperate. We
define the \textit{cooperation group} of node $v$ as the group of its neighbors
that forward the message received from $v$. For a stationary channel, the set of
nodes comprising the cooperation group is constant. We denote the cooperation
group of $v$ as $L(v)$. The node $v$ decides if the certain neighbor joins
$L(v)$ based on some function for selection of the cooperating nodes. In this
paper, we propose several such functions and compare them. The cooperation group
constructed using the particular function $F$ we denote as $L(v,F)$. We use the
following function annotations $\textit{F=\{\small AUDIBLE, USEFUL,
	FILTER\_BY\_LOSS, FILTER\_BY\_P,}$ $\textit{\small WEIGHTED, CUMULATIVE}\}$.

\subsubsection{Function \sel{AUDIBLE}}

Any network with a broadcast channel can be theoretically considered as the full
mesh network (where all nodes are interconnected). In practice, many of
point-to-point transmission are not possible due to the distance between nodes.
The maximum size of $L(v)$ depends on the number of the neighbors to which $v$
can ``talk'' to, using the most robust MCS. The group of such neighbors of $v$
we define as the \textit{audible nodes}. The cooperation group constructed in
this way we denote as $L(v,\sel{AUDIBLE})$, where AUDIBLE is the constructing
function.

\subsubsection{Function \sel{USEFUL}}

Using the maximum cooperation group size is not always the best strategy. In
\cite{approachinglimits}, the optimal function for the cooperation group
construction is defined, which we refer to as \sel{USEFUL}. We form
$L(v,\sel{USEFUL})$ out of nodes in $L(v,\sel{AUDIBLE})$ checking for each node
if it has better connection to the destination than the node $v$. The connection
to the destination can include several hops. Basically, the authors in
\cite{approachinglimits} proposes to use $p(v)$ as the connection quality
metric.

Note that $L(v,\sel{USEFUL})$ is the de facto cooperation group function in most
of the recent OpR studies including \cite{more, diss}. The following functions
are considered to be the actual shrinking criteria to reduce from
$L(v,\sel{USEFUL})$. Therefore, only these functions will be considered for the
evaluation of the CGS reduction.

\subsubsection{Function \sel{FILTER\_BY\_LOSS}}

The function \sel{USEFUL} gives the optimal set of the cooperating nodes.
However, in practice, the nodes in $L(v,\sel{USEFUL})$ can have a huge
difference in contribution on the throughput. It depends on that how actively
they cooperate. The node $v$ instructs the cooperating nodes to forward
different share of the received information. The less data should be forwarded
the less signficance the cooperating node has. In the same time, the more
cooperating nodes we have the more coordination between them is required. Having
a big cooperation group implies sending more feedback and decreasing the
protocol stability. Hence, we propose several methods to reduce the cooperation
group.

With the first method, we form $L(v,\sel{FILTER\_BY\_LOSS})$ by selection of
certain nodes out of $L(v,\sel{USEFUL})$. We sort the nodes in
$L(v,\sel{USEFUL})$ in the descending order of the loss probability
$\varepsilon_{(v,u)}$, $u \in L(v)$ and select certain number of nodes on the
top of the list. Intuitively, this method could appeal to be right since we
choose the neighbors which can decode our message with the least amount of
retransmissions. The ones that are ``farther away'' are rendered unfit to
cooperate. However, note that the connection quality $\varepsilon_{(v,u)}$ gives
only the local information about the performance of $v$, while the gain $g(v)$
takes into account the whole network connecting $v$ with the destination. As a
result, we might not select the nodes that have a bad connection to $v$ but a
good connection to the destination. Then, we may loose in the throughput.

\subsubsection{Function \sel{FILTER\_BY\_P}}

The metric $p(v)$ denotes the maximum achievable data rate between $v$ and the
destination. We also use it as the metric to sort the nodes in
$L(v,\sel{USEFUL})$. Since every node would already possess the $p(u)$ values of
all its neighbors $u\in L(v)$ \cite{approachinglimits}, no extra information
needs to be negotiated for using this metric. The reasoning behind choosing this
metric could be that we prefer the nodes which have the best link to the
destination in our cooperation group. However, this method can have a drawback.
By choosing the node with the highest priority to $w$, we tend to always choose
the nodes that are farthest away from the source $v$. In other words, we tend to
pick the nodes $u\in L(v,\sel{USEFUL})$, which have the worst link to the node
$v$.

Since the $g(v)$ depends on both $\varepsilon_{(v,u)}$ and $p(u)$, the best
sorting method should take both of them into account. The following two methods
do exactly that.

\subsubsection{Function \sel{WEIGHTED}}

The first intuitive combination of the above two parameters results in the
\textit{weighted air time} of $u\in L(v)$: \[ \beta(u,v)=b(u,v)/p(u), \] where
the weighting coefficient $b(u,v)$ can be evaluated with eq.~\ref{eq:aandb}. You
can observe that $b(u,v)$ describes the mutual loss process on a certain subset
of edges between $v$ and $L(v)$. Hence, it considers $\varepsilon_{(v,u)}$.
Remember that $b(u,v)$ evaluates the number of packets that will be forwarded by
$u$ per one packet sent by $v$ (it has the floating point and can be $<1$). With
a view to consider the unequal channel access time by the nodes in $L(v)$, we
multiply the air time $p(u)^{-1}$ by $b(u,v)$. The channel access time can be
unequal e.g. due to the filtering routing rule (that uses $b(u,v)$). Hence, the
sorting the nodes in the ascending order of $\beta(u,v)$ considers the whole
network subgraph from $v$ to the destination.

Indeed, sorting by $\beta(u,v)$ it seems that we first remove smaller parts from
the sum in the denominator of the priority equation eq.~\ref{eq:prioirtymain}.
In fact, after removing each $u$, the $\beta(u,v)$ values are recalculated and
the order of $u$ after sorting by $\beta(u,v)$ can change. Thus, there is no
guarantee that the gradient of $g(v)$ is steadily increasing.

\subsubsection{Function \sel{CUMULATIVE}}

We propose one more intuitive method that takes into account both
$\varepsilon_{(v,u)}$ and $p(u)$. We sort the nodes based on the descending
order of the airtime that they evoke on the overall flow.
The coefficient
$k(u,v)$:
\begin{equation}
k(u,v)=1/p(u) + 1/[d(v)\cdot (1 - \varepsilon_{(v,u)})]
\end{equation}
is the sum of the end-to-end air time of $u$ and the air time of $v$ on the edge
$(v,u)$. With other words, $k(u,v)$ is the air time of $v$ if it has only one
node in its cooperation group $L(v)=\{u\}$. We refer to $k(u,v)$ as the
\textit{cumulative air time}. In difference to the previous method, here we do
not consider the cooperation factor of $u\in L(v)$. Thus, removing one node from
$L(v)$ does not require the new sorting of the remaining nodes or any other
recalculations like it is required when sorting by $\beta(u,v)$. Thus, the
computational complexity is also significantly reduced.

\section{Performance evaluation}

The selector functions proposed above imply different number of cooperating
nodes. The highest number is achieved with the function \sel{AUDIBLE}. A smaller
number is achieved by the function \sel{USEFUL}. In
fig.~\ref{fig:cdf_coopgroupsize}, we show the Cumulative Density Function (CDF)
of $L(v,\sel{AUDIBLE})$ and $L(v,\sel{USEFUL})$. % We evaluated the cooperation
% group size for each pair of the sending node $v$ and the destination $w\in V$.

\begin{figure}[!t]
\centering
    \includegraphics[width=0.89\columnwidth]{imgs/plot1-cdf_listeners.pdf}
\caption[caption]{CDF of cooperation group size (for each pair of $v\in V$ 
and destination $w\in V\setminus v$)}
\label{fig:cdf_coopgroupsize}
\end{figure}

In this section, we evaluate how the choice of reduction functions
$\sel{FILTER\_BY\_LOSS, FILTER\_BY\_P, WEIGHTED, CUMULATIVE}$ has varied effects
on the resulting OpR gain. Naturally, the most favorable  choice would be where
the $L(v)$ can be shrinked to a very small group with least reduction in the OpR
gains.


\subsection{Contribution of cooperating nodes}
\label{ss: contrib-neighbors}

Say, $L(v)=L(v,\sel{AUDIBLE})$. We evaluate the change of the channel capacity
between $v$ and $L'(v)$, where $L'(v)$ is a certain subset of $L(v)$. Surely,
the channel capacity between $v$ and $L'(v)$ decreases when the size of $L'(v)$
decreases. Nevertheless, for a sufficiently big $L'(v)$ the change of the
capacity is not significant. Consequently, the achievable data rate does not
change significantly.

We evaluate the change in the channel capacity as follows. The Packet Erasure
Channel (PEC) capacity between $v$ and the first $u$ in $L(v)$ is equal
$C_u=d(v)\cdot (1 - \varepsilon_{(v,u)})$ \cite{Cover2006}, where $d(v)$ is the
sending data rate corresponding to the selected MCS. The PEC capacity between
$v$ and the whole group $L'(v)$ for uncorrelated channels is equal
$C_{L'(v)}=d(v)\cdot (1 - \prod_{u\in L'(v)}\varepsilon_{(v,u)})$. In reality,
the channels between $v$ and each $u\in L'(v)$ are correlated. We consider it
using our measurement database as follows. We save more detailed information
than the PER values. With $s[v][k][u][j][i]$, we can check the reception status
of each packet at each node (see section~\ref{sec:mnat}). Using $s[v][k][u]$ for
all $j$ and $i$, we form the bit sequences $b[v][k][u]$. As described above, the
MCS $k$ is the function of $v$ and $u$. Therefore, we adopt the notation
$bs_{(v,u)}=b[v][k][u]$ for simplicity. Remember that $bs_{(v,u)}[i]=1$ means
that the CRC check of the $i$the packet is positive. Then:
\begin{equation}
C_{L'(v)}=d(v)\cdot [1 - (1 -||\cup_{u\in L'(v)}bs_{(v,u)}||^2/l)],
\end{equation}
where all $bs_{(v,u)}$ have equal length $l$ and $||x||^2$ is the norm of a
certain binary vector $x$ (in our measurement $l= 12000$).

We calculate $C_{L'(v)}$ for each $L'(v)$ and for each $v$. The set $L'(v)$ is
obtained from $L(v)$ using the selector function \sel{FILTER\_BY\_LOSS}. Since
we have 15 routers in our testbed, we can obtain up to 14 different $L'(v)$ for
each $v$ out of 15 nodes. In fig.~\ref{fig:alphacontribution}, we show the value
$C_{L'(v)}$ normalized by $C_{L(v)}$:
\begin{equation} \alpha(v)=C_{L'(v)}/C_{L(v)}. \end{equation}
The coefficient $\alpha(v)$ gives the portion of information that flows through
the subset $L'(v)$ out of the information that flows through $L(v)$. We observe
that for any $v\in V$, 99.9\% of information flows through the first 6 nodes in
$L(v)$. Therefore, using 6 neighbors for cooperation is sufficient. Comparing to
fig.~\ref{fig:cdf_coopgroupsize}, we see that the optimal cooperation group
$L(v,\sel{USEFUL})$ in 23\% of cases is greater than 6. Hence, it makes sense to
apply more aggressive selector functions to reduce the number of the cooperating
nodes even more.

\begin{figure}[!t]
\centering
    \includegraphics[width=1.0\columnwidth]{imgs/plot2-useful-overhearers.pdf}
\caption[caption]{Contribution of different nodes in overall performance 
using the cooperation groups $L(v)$}
\label{fig:alphacontribution}
\end{figure}

\subsection{Gain evaluation}

For gain calculation, we use the eq.~\ref{eq:gain}. In order to achieve the
highest gain of OpR over SPR, we should use the selector function \sel{USEFUL}.
The CDF of such gain is shown in fig.~\ref{fig:maxgain}. As it has been already
proved in \cite{approachinglimits}, the gain is always positive.

\begin{figure}[!t]
\centering
    \includegraphics[width=1.0\columnwidth]{imgs/cdf_gain.pdf}
\caption[caption]{Maximum gain of OpR over SPR in our testbed}
\label{fig:maxgain}
\end{figure}

Before showing the overall average results, we pick a pair of source and
destination nodes (correspondingly nodes 7 and 1 in fig.~\ref{fig:topology}). In
fig.~\ref{fig:gainsforonepair}, we show the gain using the selector functions
\sel{FILTER\_BY\_LOSS, FILTER\_BY\_P, WEIGHTED, CUMULATIVE}. The gain is plotted
as a function of the cooperation group size $|L(v)|$. For $|L(v)|\geq 6$, the
gain value is the same as for the selector \sel{USEFUL}. For smaller $|L(v)|$,
it starts dropping since we reduce the spatial channel diversity removing more
nodes significantly.

\begin{figure}[!t]
\centering
\includegraphics[width=1.0\columnwidth]{imgs//plot4-prio-Z.pdf}

\caption[caption]{Gain of OpR over SPR for the selected pair of sender $v$ and
    destination $w$; $v=7$, $w=1$(see testbed topology in fig.~\ref{fig:topology})}
\label{fig:gainsforonepair}
\end{figure}

In fig.~\ref{fig:averagegains}, we see the analog results but averaged for all
possible pairs of source and destination nodes. It can be seen that the average
OpR gain of the entire network is around 1.14. The best performance is achieved
with the selector function \sel{CUMULATIVE}. Sorting with \sel{CUMULATIVE}, we
never get the negative gain ($<1$). Moreover, it consistently and significantly
outperforms all the other sorting mechanisms. Hence, we regard it as the best
sorting method. With this selector, it is possible to use \textit{only two}
cooperating nodes per sender on average to achieve a gain of 1.11. In other
words, by limiting the maximum CGS down to two using \sel{CUMULATIVE}, we only
lose 3\% of the average gain.

\begin{figure}[!t]
\centering
    \includegraphics[width=\columnwidth]{imgs/plot6-bar-gain-maxG.pdf}
\caption[caption]{Average Gain of OpR over SPR with 95\% confidence interval}
\label{fig:averagegains}
\end{figure}


\section{Conclusions}
\label{sec:conclusions}

Application of OpR allows using the channel resources more effectively through
the use of multiple cooperating nodes aiding the flow. The greater number of
cooperating nodes can also lead to the decrease in stability of the routing
protocol and transfer of out-dated or redundant packets. With a view to reducing
these unwanted effects, we proposed several methods for reduction of the
cooperation group sizes.

We conducted the channel measurement in the office building with a network of 15
WiFi nodes. The obtained traces was used in post-processing for evaluation of
potential cooperation group sizes. We have shown that the size of the audible
cooperation group can reach the maximum in this testbed ($|V| - 1 = 14$).
Nevertheless, simply the first 6 routers in each cooperation group can already
recover 99.9\% of the information, i.e. the meaningful size of the cooperation
group can not be greater than 6 (less than 50\% of the original group size).
Then, we analyzed the methods to reduce this size even further. We
designed a performance metric that evaluates the change of the end-to-end data
rate when we remove the cooperating nodes. Theoretically, the gain will always
decrease. Practically, however, we have shown that the decrease will be not
significant if we remove the ``right'' nodes. We propose and evaluate four
different methods of removing the nodes. The best performance is shown when
sorting the nodes in the cooperation group by the \textit{cumulative air time} 
\sel{CUMULATIVE}. In our testbed, it is sufficient to use only three nodes for
the cooperation and still achieve over 95\% of the original gain as attained by
classical OpR protocols.

The corresponding software that was developed for the testbed can also serve for
exploration of the OpR potential in any mesh network. With the open source
scripts, the measurement can be repeated on any test field. The post-processing
scripts evaluate the theoretically maximum gain of OpR over SPR and allow to
investigate the effects of shrinking the cooperation group.

\bibliographystyle{IEEEtran}
\balance
\bibliography{main}


\end{document}
